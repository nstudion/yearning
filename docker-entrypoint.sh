#!/usr/bin/env bash

set -e

if [ -z $DATABASE ]
then
    export DATABASE=yearning
fi
sed -i 's/${DATABASE}'"/$DATABASE/g" conf.toml

if [ -z $DB_HOST ]
then
    export DB_HOST=172.17.0.1
fi
sed -i 's/${DB_HOST}'"/$DB_HOST/g" conf.toml

if [ -z $DB_PORT ]
then
    export DB_PORT=3306
fi
sed -i 's/${DB_PORT}'"/$DB_PORT/g" conf.toml

if [ -z $DB_USER ]
then
    export DB_USER=root
fi
sed -i 's/${DB_USER}'"/$DB_USER/g" conf.toml

if [ -z $DB_PASSWORD ]
then
    export DB_PASSWORD=P@ssowrd
fi
sed -i 's/${DB_PASSWORD}'"/$DB_PASSWORD/g" conf.toml

/app/Yearning install
/app/Yearning migrate
/app/Yearning reset_super

exec "$@"



