# READ ME
This version is ref from offical version。
https://yearning.io/

## Init SQL
```
create database `${yearning database name}` default character set utf8mb4 collate utf8mb4_unicode_ci;
```

## Docker build
```
git clone git@gitlab.com:nstudion/yearning.git
docker build -t ${name} .
```

## Docker RUN Environment

### DATABASE 
yearning use databases name

Default: yearning

### DB_HOST
yearning use databases host 

Default: 172.17.0.1

### DB_PORT
yearning use databases port

Default: 3306

### DB_USER
yearning use databases user

Default:root
### DB_PASSWORD
yearning use databases password

Default:P@ssword
## Docker RUN Example
```
docker run -d --name ${CONTAINER NAME} \
    -p 8000:8000 \
    -e DATABASE="${DATABASE}" \
    -e DB_HOST="${DB_HOST}" \
    -e DB_PORT="${DB_PORT}" \
    -e DB_USER="${DB_USER}" \
    -e DB_PASSWORD="${DB_PASSWORD}" \
    ${image name}
```

## Note
The conf.toml don't change . Just set value with docker run