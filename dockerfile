FROM ubuntu:22.04

WORKDIR /app

COPY ./Yearning /app
COPY ./migrate /app/
COPY ./conf.toml /app/
COPY docker-entrypoint.sh /

RUN chmod 775 /docker-entrypoint.sh


RUN apt-get update && apt-get install -y git \
    && apt-get install -y golang

EXPOSE 8000

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["/app/Yearning", "run"]
